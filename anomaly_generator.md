# Anomaly (Use Case) Generator

The Quote of the Day Anomaly Generator is a separate Node.js web application that allows you to initiate anomalous behavior in any of the QotD services. Out of the box, the generator app comes with some built in anomalies, but you can add your own.

## JSON Use Case Definition

Each use case is defined by JSON object.  The object has an identifier, name, description and an array of steps.  The identifier is any string (that can be used as a linux filename), and must be unique across all stored use cases.

```JSON
{
    "id": "a_ratings_failure",
    "name": "Rating service failure",
    "description": "This use case simulates a failure in the ratings service.",
    "steps": [
        ...
    ]
}
```

Each step describes an action that adjusts a varaibility point in the operation of the service.  These are not simulated actions.  If the varability point is to increase CPU usage, then the service is instructed to spend a lot of additional CPU cycles (computing random numbers) to the point that the increase is noticeable by the metrics collection system (i.e. Prometheus).

Variabilty Points
- CPU usage
- Memory usage
- Latency in app APIs
- New log messages

## Step types

Each step object specified a `name` (which is used in the user interface as a type of description), and a `service` target.  The current set of service targets include `web`, `quote`, `rating`, `author`, `image`, and `pdf`.  The actual service endpoints are spcified in environment variabes for this application at runtime, so these scripts are portable across application instances.

Depending upon the type of step there may be additional parameters supplied.  The format and name of the parameters varies according to the step type.

### Delay

A delay step pauses the execution of the use case by a generated number of milliseconds.

Example Delay:
```JSON
{
    "name": "Pause for about 2.5 seconds",
    "type": "delay",
    "service": "rating",
    "duration": 2500
}
```

### Metrics

A metric step adjusts one of CPU, Memory or API latency, specified by the `metric` property.  The metric property must be one of `mem`, `cpu` or `api`.  The `value` property varies acording to the type of metric.

For CPU and Memory, the measurement is in a arbitary unit called hogs (i.e. "cpu hog", "memory hog"), which do nothing more than consume resources. In the case of CPU, each hog goes off and computes millions of random numbers.  For memory each hog allocates an array and fills it will numbers (hence there will be a small CPU spike when adding new memory hogs).  In practice the actual values used are scaled such that a value of 5 hogs will induce a significantly noticeble change.  A value of 8 is probably the maximum amount you can specify without risking the crashing of the application (pod, container, or when run natively).  The valid values are 0 to 10.

For latency each main service REST call is already delayed by 100ms (by default).  This can be increased up to about 10 seconds (self imposed limit).  The actual delay in each instance is generated so there is some variablilty.  The generated value is from a normal distribution with a mean, standard deviation, min and max values.  The units are in milliseconds.

Example Memory:
```JSON
{
    "name": "Increase memory usage",
    "type": "metric",
    "service": "rating",
    "metric": "mem",
    "value": {
        "hogs": 8
    }
}
```

Example CPU:
```JSON
{
    "name": "Increase cpu usage",
    "type": "metric",
    "service": "author",
    "metric": "cpu",
    "value": {
        "hogs": 5
    }
}
```

Example Latency:
```JSON
{
    "name": "Increase quote latency",
    "type": "metric",
    "service": "quote",
    "metric": "api",
    "value": {
        "mean": 2000,
        "stdev": 200,
        "min": 200,
        "max": 1100
    }
}
```

### Reset Metrics

You can reset any given metric to its factory default settings (the values used when generating normal training data). Since the factory settings are already known to the service no values are needed with this step.

Example Reset CPU:
```JSON
{
    "name": "Reset cpu usage",
    "type": "metric",
    "service": "author",
    "metric": "cpu"
}
```

### Service Actions

A service action step sends service wide commands like; `stop`, `start`, `crash` and for kubernetes deployments control the Liveness probe.

Example Crash Service:
```JSON
{
    "name": "Crash author service",
    "type": "service",
    "service": "author",
    "action": "crash"
}
```

Example Stop a Running Service:
```JSON
{
    "name": "Stop author service",
    "type": "service",
    "service": "author",
    "action": "stop"
}
```

Example Start a Stopped Service:
```JSON
{
    "name": "Start author service",
    "type": "service",
    "service": "author",
    "action": "start"
}
```

Example Make Liveness probe fail (i.e. return 500 status):
```JSON
{
    "name": "Make Liveness probe fail",
    "type": "service",
    "service": "author",
    "action": "sick"
}
```

Example Make Liveness probe return OK:
```JSON
{
    "name": "Make Liveness probe return OK",
    "type": "service",
    "service": "author",
    "action": "heal"
}
```

### Add Logger

New loggers can be added to a service.  These logs are not tied to any specific input call, but rather are repeated.  The frequency is specified by a delay between calls.  The delay is computed with a normally distributed random value (mean, std dev, min, max). Fields in each log entry can be varied in several different ways with faked data.

The parameters for the log include an `id` which is used later to remove the logger when desired.  The `name` is optional and used when querying active loggers in a service.  The `template` is a string that contains the text of the log entry.  This string may contain self-defined variables and are surrounded by a pair of double curley braces.  Each defeined variable is expected to be mentioned in the `fields` property.

Each property of `fields` defines a variable type, and any additional parameters, that will produce a string which is substituted in the template.  Many of these types come from the [Faker](https://www.npmjs.com/package/faker) module, which produces realistic fake data for testing.

**Pick** `pick`

The field will be replaced with a value from one of the randomly chosen (flat distribution) options.  

Example:
```JSON
"log": {
    "id": "log1",
    "name": "memory checksum",
    "template": "WARNING - computation option {{OPTION}} not valid.",
    "fields": {
        "OPTION": {
            "type": "pick",
            "options": [ "op1", "op2", "op3" ]
        }
    },
    "repeat": {
        "mean": 4000,
        "stdev": 1000,
        "min": 2000,
        "max": 8000
    }
}
```

The resulting log entry might look like:

```
WARNING - computation option op1 not valid.
```

**Pick Weighted** `pickWeighted`

Similar to the **pick** field type, a weighted pick allows the selection to be weighted.  Each option is assigned a weight.  The algorithm used to select the value for any given instance of the generated log is to create a slots for each option, with weight being the number of slots.  Then only one of the slots is selected at random.  The more slots associated with an option, the more likley it will be selected.

For example suppose the weighted options are:

```JSON
"options": [
    { "value": "GET", "weight": 8 },
    { "value": "PUT", "weight": 1 },
    { "value": "POST", "weight": 1 },
    { "value": "DELETE", "weight": 2 }
]
```

The total weight is 12.  This means that 8 times out of 12 the value `GET` will be selected.  One time out of 12 the `PUT` or `POST` value will be selected.

Example New Logger:

```JSON
{
    "name": "Start new repeating log warning about memory checksum every 2 seconds",
    "type": "add_logger",
    "service": "rating",
    "log": {
        "id": "log1",
        "name": "memory checksum",
        "template": "CALL to external service returned an HTTP status {{ERROR}}. ",
        "fields": {
            "ERROR": {
                "type": "pickWeighted",
                "options": [
                    { "value": "400", "weight": 5 },
                    { "value": "401", "weight": 2 },
                    { "value": "404", "weight": 2 },
                    { "value": "500", "weight": 1 }
                ]
            }
        },
        "repeat": {
            "mean": 3000,
            "stdev": 1000,
            "min": 1000,
            "max": 5000
        }
    }
}
```

**Normal Int** `normalInt`

Generates a integer value based off a normal curve.  The mean, std deviation, min and max are specified.

Example:

```JSON
{
    "name": "Sample log entry",
    "type": "add_logger",
    "service": "rating",
    "log": {
        "id": "log1",
        "name": "outside temp",
        "template": "The outside temperature is a nice {{temp}}F. ",
        "fields": {
            "temp": {
                "type": "normalInt",
                "mean": 70,
                "stdev": 10,
                "min": 60,
                "max": 90
            }
        },
        "repeat": {
            "mean": 3000,
            "stdev": 1000,
            "min": 1000,
            "max": 5000
        }
    }
}
```

**Normal Float** `normalFloat`

Generates a numeric value from a normal distribution with the given mean and standard deviation.  The optional parameter `toFixed` is applied to the resulting value to fix the number of decimal places in the output.

Example:

```JSON
{
    "name": "Sample log entry",
    "type": "add_logger",
    "service": "rating",
    "log": {
        "id": "log1",
        "name": "outside temp",
        "template": "The outside temperature is a nice {{temp}}F. ",
        "fields": {
            "temp": {
                "type": "normalFloat",
                "mean": 70,
                "stdev": 10,
                "min": 60,
                "max": 90,
                "toFixed": 2
            }
        },
        "repeat": {
            "mean": 3000,
            "stdev": 1000,
            "min": 1000,
            "max": 5000
        }
    }
}
```

**Random Int** `randomInt`

Generates a random integer from a flat distribution.  Parameters `min` and `max` set the range (inclusive).

Example:

```JSON
{
    "name": "Sample log entry",
    "type": "add_logger",
    "service": "rating",
    "log": {
        "id": "log1",
        "name": "customer id number",
        "template": "Failed accessing customer ID: {{custId}} ",
        "fields": {
            "custId": {
                "type": "randomInt",
                "min": 1000000,
                "max": 9999999
            }
        },
        "repeat": {
            "mean": 3000,
            "stdev": 1000,
            "min": 1000,
            "max": 5000
        }
    }
}
```


**Random Float** `randomFloat`

Generates a random number with decimal places.  The params `min` and `max` set the range (inclusive).  The optional parameter `toFixed` will set the number of decimal places.


Example:

```JSON
{
    "name": "Sample log entry",
    "type": "add_logger",
    "service": "rating",
    "log": {
        "id": "log1",
        "name": "customer id number",
        "template": "Failed accessing customer ID: {{custId}} ",
        "fields": {
            "custId": {
                "type": "randomFloat",
                "min": 1000000,
                "max": 9999999
            }
        },
        "repeat": {
            "mean": 3000,
            "stdev": 1000,
            "min": 1000,
            "max": 5000
        }
    }
}
```

**IP** `ip`

Generates a random IP address (i.e. 169.14.199.4).

```JSON
{
    "name": "Sample log entry",
    "type": "add_logger",
    "service": "rating",
    "log": {
        "id": "log1",
        "name": "threat warning",
        "template": "Unexpected service access from {{IP}} ",
        "fields": {
            "IP": {
                "type": "ip"
            }
        },
        "repeat": {
            "mean": 3000,
            "stdev": 1000,
            "min": 1000,
            "max": 5000
        }
    }
}
```

**Email Address** `email`

Generates a generic email address.

```JSON
{
    "name": "Sample log entry",
    "type": "add_logger",
    "service": "rating",
    "log": {
        "id": "log1",
        "name": "new user",
        "template": "Created new user account for {{newEmail}}",
        "fields": {
            "newEmail": {
                "type": "email"
            }
        },
        "repeat": {
            "mean": 3000,
            "stdev": 1000,
            "min": 1000,
            "max": 5000
        }
    }
}
```

**User Name** `userName`

Generates an example user name (i.e. name following typical name guidelines like no spaces, or control characters, etc.)

```JSON
{
    "name": "Sample log entry",
    "type": "add_logger",
    "service": "rating",
    "log": {
        "id": "log1",
        "name": "new user",
        "template": "User {{user}} requested access to a forbidden directory.",
        "fields": {
            "user": {
                "type": "userName"
            }
        },
        "repeat": {
            "mean": 3000,
            "stdev": 1000,
            "min": 1000,
            "max": 5000
        }
    }
}
```

**URL** `url`

Generates a generic URL.

```JSON
{
    "name": "Sample log entry",
    "type": "add_logger",
    "service": "rating",
    "log": {
        "id": "log1",
        "name": "access url warning",
        "template": "Unable to access arbitrary URL: {{aurl}}.",
        "fields": {
            "aurl": {
                "type": "url"
            }
        },
        "repeat": {
            "mean": 3000,
            "stdev": 1000,
            "min": 1000,
            "max": 5000
        }
    }
}
```

**MAC Address** `mac`

Generates a sample MAC address.

```JSON
{
    "name": "Sample log entry",
    "type": "add_logger",
    "service": "rating",
    "log": {
        "id": "log1",
        "name": "access MAC warning",
        "template": "Duplicate MAC value warning {{dupMac}}.",
        "fields": {
            "dupMac": {
                "type": "mac"
            }
        },
        "repeat": {
            "mean": 3000,
            "stdev": 1000,
            "min": 1000,
            "max": 5000
        }
    }
}
```

**Domain Name** `domainName`

Generates a sample domain name.

```JSON
{
    "name": "Sample log entry",
    "type": "add_logger",
    "service": "rating",
    "log": {
        "id": "log1",
        "name": "Unavailable domain name",
        "template": "Who would have thought the domain name {{dn}} would be taken already.",
        "fields": {
            "dn": {
                "type": "domainName"
            }
        },
        "repeat": {
            "mean": 3000,
            "stdev": 1000,
            "min": 1000,
            "max": 5000
        }
    }
}
```

**Word** `word`

Generates a nonsensical word (pig latin).  

```JSON
{
    "name": "Sample log entry",
    "type": "add_logger",
    "service": "rating",
    "log": {
        "id": "log1",
        "name": "Baby name",
        "template": "Only a billionare would name thier child {{name}}.",
        "fields": {
            "name": {
                "type": "word"
            }
        },
        "repeat": {
            "mean": 3000,
            "stdev": 1000,
            "min": 1000,
            "max": 5000
        }
    }
}
```

**Sentence** `sentence`

Generates a sentence worth of nonsensical words.

```JSON
{
    "name": "Sample log entry",
    "type": "add_logger",
    "service": "rating",
    "log": {
        "id": "log1",
        "name": "strange comment",
        "template": "User submitted comment {{comment}}.",
        "fields": {
            "comment": {
                "type": "sentence"
            }
        },
        "repeat": {
            "mean": 3000,
            "stdev": 1000,
            "min": 1000,
            "max": 5000
        }
    }
}
```

**Recent Date** `dateRecent`

Generates a recent date (something within days).  The format of the date time can be specified with a string that corresponds to the [Moment.js formats](https://momentjs.com/docs/#/displaying/format/).


```JSON
{
    "name": "Sample log entry",
    "type": "add_logger",
    "service": "rating",
    "log": {
        "id": "log1",
        "name": "order fulfilled",
        "template": "Order fulfilled on {{dt}}.",
        "fields": {
            "dt": {
                "type": "dateRecent",
                "format": "dddd, MMMM Do YYYY, h:mm:ss a"
            }
        },
        "repeat": {
            "mean": 3000,
            "stdev": 1000,
            "min": 1000,
            "max": 5000
        }
    }
}
```

**Formatted Current Timestamp** `formattedTimestamp`

Generates the current timestamp, and allows for formatting with [Moment.js formats](https://momentjs.com/docs/#/displaying/format/).

```JSON
{
    "name": "Sample log entry",
    "type": "add_logger",
    "service": "rating",
    "log": {
        "id": "log1",
        "name": "xaction completed",
        "template": "Transaction completed on {{now}}.",
        "fields": {
            "now": {
                "type": "dateRecent",
                "format": "dddd, MMMM Do YYYY, h:mm:ss a"
            }
        },
        "repeat": {
            "mean": 3000,
            "stdev": 1000,
            "min": 1000,
            "max": 5000
        }
    }
}
```

**UTC Timestamp** `utcTimestamp`

Generated the current time in UTC format.

```JSON
{
    "name": "Sample log entry",
    "type": "add_logger",
    "service": "rating",
    "log": {
        "id": "log1",
        "name": "esperanto now",
        "template": "Transaction completed on {{now}}.",
        "fields": {
            "now": {
                "type": "utcTimestamp"
            }
        },
        "repeat": {
            "mean": 3000,
            "stdev": 1000,
            "min": 1000,
            "max": 5000
        }
    }
}
```

**Filename** `fileName`

Generates a sample filename.

```JSON
{
    "name": "Sample log entry",
    "type": "add_logger",
    "service": "rating",
    "log": {
        "id": "log1",
        "name": "file missong",
        "template": "Unable to access file: {{fn}} in current directory.",
        "fields": {
            "now": {
                "type": "fileName"
            }
        },
        "repeat": {
            "mean": 3000,
            "stdev": 1000,
            "min": 1000,
            "max": 5000
        }
    }
}
```

**Filetype** `fileType`

Generates a known file type extension (i.e. png, pdf, gif, pptx)

```JSON
{
    "name": "Sample log entry",
    "type": "add_logger",
    "service": "rating",
    "log": {
        "id": "log1",
        "name": "file ext not accepted",
        "template": "File types with {{ext}} are not accepted here.",
        "fields": {
            "now": {
                "type": "fileType"
            }
        },
        "repeat": {
            "mean": 3000,
            "stdev": 1000,
            "min": 1000,
            "max": 5000
        }
    }
}
```

### Examples

Example: a SQL like log message.

```JSON
{
    "name": "Request SQL",
    "id": "request-sql",
    "delay": {
        "min": 100,
        "max": 10000,
        "skew": 1
    },
    "template": "{{TIMESTAMP}} Query: SELECT * FROM {{TABLE}} WHERE {{TABLE}}_id='{{VALUE}}' {{RETURN_CODE}} User: {{USERID}} Source: {{IP}}",
    "fields": {
        "TIMESTAMP": {
            "type": "utcTimestamp"
        },
        "TABLE": {
            "type": "pickWeighted",
            "options": [
                { "value": "users", "weight": 8 },
                { "value": "catalog", "weight": 1 },
                { "value": "inventory", "weight": 1 },
                { "value": "comments", "weight": 2 }
            ]
        },
        "VALUE": {
            "type": "randomInt",
            "min": 100000,
            "max": 999999
        },
        "RETURN_CODE": {
            "type": "pickWeighted",
            "options": [
                { "value": "({{ROWS}}) rows returned", "weight": 10 },
                { "value": "null", "weight": 2 },
                { "value": "error", "weight": 1 }
            ]
        },
        "ROWS": {
            "type": "randomInt",
            "min": 1,
            "max": 100
        },
        "USERID": {
            "type": "userName"
        },
        "IP": {
            "type": "ip"
        }
    }
}
```

# Adding and Removing Use Cases

Use cases must be defined as JSON objects, as described above.  The **Manager** page allows you to upload new use case definitions, and remove existing ones.

Pleas note that at the moment any uploaded use case are stored in the local pod.  Should the pod get destroyed and replaced, any added use cases will be lost (so keep a copy locally).

