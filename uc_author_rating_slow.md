# Anomaly: Author and Rating Services Slow

This use case executes the following steps:

1. Two new types of log entries are introduced in the Author and Rating service.  The first is of the form:

    ```
    Incoming request for resource {{URL}} source: {{IP}} 
    ```
    It repeats about every 3 seconds.

    The second of the form:

    ```
    WARNING incoming {{VERB}} request from {{IP}} rejected 
    ```
    It repeats about every 3 seconds.

    The use case pauses for 2 minutes.

2. The Author and Rating service latency is set to delay 2 seconds nominally.




## Source

```json
{
    "id": "image_author_slow",
    "name": "Author and Image Service Slowage",
    "description": "New log entries are added to author and image services.  A minute later they both experience harp increases in CPU and memory usage. The response time of their primary services near 2 seconds.",
    "steps": [
        {
            "name": "Start log warning about requests for resource fro IP address (author service).  Repeats every 3 seconds.",
            "type": "add_logger",
            "service": "author",
            "log": {
                "id": "resreq1",
                "name": "dangerwill",
                "template": "DANGER Will Robinson. Unexpected request for {{URL}} from source: {{IP}} ",
                "fields": {
                    "URL": {
                        "type": "url"
                    },
                    "IP": {
                        "type": "ip"
                    }
                },
                "repeat": {
                    "mean": 3000,
                    "stdev": 100,
                    "min": 2000,
                    "max": 4000
                }
            }
        },
        {
            "name": "Start log warning about requests for resource fro IP address (image service).  Repeats every 3 seconds.",
            "type": "add_logger",
            "service": "image",
            "log": {
                "id": "resreq1",
                "name": "possible hack",
                "template": "ERROR possible hacking to access resource {{URL}} from source: {{IP}} ",
                "fields": {
                    "URL": {
                        "type": "url"
                    },
                    "IP": {
                        "type": "ip"
                    }
                },
                "repeat": {
                    "mean": 3000,
                    "stdev": 100,
                    "min": 2000,
                    "max": 4000
                }
            }
        },
        {
            "name": "Start log warning about incoming HTTP requests from external IP address (author service).  Repeats about every second.",
            "type": "add_logger",
            "service": "author",
            "log": {
                "id": "in_req",
                "name": "warn incoming",
                "template": "WARNING incoming {{VERB}} request from {{IP}} rejected ",
                "fields": {
                    "IP": {
                        "type": "ip"
                    },
                    "VERB": {
                        "type": "pickWeighted",
                        "options": [
                            { "name": "GET", "value": "GET", "weight": 8 },
                            { "name": "POST", "value": "POST", "weight": 3 },
                            { "name": "PUT", "value": "PUT", "weight": 1 },
                            { "name": "DELETE", "value": "DELETE", "weight": 1 },
                            { "name": "HEAD", "value": "HEAD", "weight": 7 }
                        ]
                    }
                },
                "repeat": {
                    "mean": 1000,
                    "stdev": 500,
                    "min": 10,
                    "max": 3000
                }
            }
        },
        {
            "name": "Start log warning about incoming HTTP requests from external IP address (author service).  Repeats about every second.",
            "type": "add_logger",
            "service": "image",
            "log": {
                "id": "in_req",
                "name": "warn incoming",
                "template": "WARNING incoming {{VERB}} request from {{IP}} rejected ",
                "fields": {
                    "IP": {
                        "type": "ip"
                    },
                    "VERB": {
                        "type": "pickWeighted",
                        "options": [
                            { "name": "GET", "value": "GET", "weight": 8 },
                            { "name": "POST", "value": "POST", "weight": 3 },
                            { "name": "PUT", "value": "PUT", "weight": 1 },
                            { "name": "DELETE", "value": "DELETE", "weight": 1 },
                            { "name": "HEAD", "value": "HEAD", "weight": 7 }
                        ]
                    }
                },
                "repeat": {
                    "mean": 1000,
                    "stdev": 500,
                    "min": 10,
                    "max": 3000
                }
            }
        },
        {
            "name": "Increase rating service delay (2s)",
            "type": "metric",
            "service": "author",
            "metric": "api",
            "value": {
                "mean": 2000,
                "stdev": 200,
                "min": 200,
                "max": 1100
            }
        },
        {
            "name": "Increase author service delay (2s)",
            "type": "metric",
            "service": "image",
            "metric": "api",
            "value": {
                "mean": 2000,
                "stdev": 200,
                "min": 200,
                "max": 1100
            },
            "ramp": 100000
        }
    ]
}
```